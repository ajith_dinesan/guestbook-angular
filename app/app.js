(function(angular) {
  'use strict';
angular.module('app', ['ngComponentRouter', 'guestbook'])

.value('$routerRootComponent', 'app')

.component('app', {
  template:
    '<div class="container-fluid" ><nav>\n' +
    '</nav>\n' +
    '<ng-outlet></ng-outlet>\n </div>',
  $routeConfig: [
    {path: '/guestbook/...', name: 'Guestbook', component: 'guestbook', useAsDefault: true }
  ]
});
})(window.angular);
