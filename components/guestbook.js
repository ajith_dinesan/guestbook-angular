(function(angular) {
  'use strict';
angular.module('guestbook', [])

  .component('guestbook', {
    template: '<div class="container"><h2>Comments</h2><ng-outlet></ng-outlet><div>',
    $routeConfig: [
      {path: '/view', name: 'CommentList',   component: 'commentList', },
      {path: '/create', name: 'CreateComment', component: 'commentAdd', useAsDefault: true}
    ]
  })

  .component('commentList', {
    template:
    '<div>' +
    '<table>' +
    '<tr><th>Name</th><th>Email</th><th>Comment</th></tr>' +
    '<tr ng-repeat="comment in $ctrl.comments | orderBy: '+"'-date'"+'">' +
    '<td>{{comment.name}}</td>' +
    '<td>{{comment.email}}</td>' +
    '<td>{{comment.comment}}</td>' +
    '</tr>' +
    '</table><br><br>' +
    '<button class="primary btn btn-default" ng-link="[\'CreateComment\']" type="button">Add Comment</button>' +
    '</div>',
      bindings: { $router: '<' },
    controller: commentList
  })

  .component('commentAdd', {
    templateUrl: 'components/addComment.html',
    bindings: { $router: '<' },
    controller: commentAdd
  });

function commentList() {
  var $ctrl = this;
  $ctrl.comments = JSON.parse(localStorage.getItem("comment"));
}

function commentAdd() {
  var $ctrl = this;

  $ctrl.maxCommentLength = maxCommentLength;
   $ctrl.comments = JSON.parse(localStorage.getItem("comment"));

   this.checkValue = function(comment) {
    if (comment==undefined || comment.name == undefined || comment.email == undefined || comment.comment == undefined)
    {
      alert("Please provide all fields / provide valid data");
    } else {
      this.onSubmit(comment);
    }
   }

  this.onSubmit = function(comment) {

    var array = [];
    var err = false;
    comment['date'] = new Date().getTime();
    if (localStorage.getItem("comment") != null) {
      array = JSON.parse(localStorage.getItem("comment"));
      var result = array.filter(obj => {
        return obj.email === comment.email
      })
      if (result.length) err = true;
      else err = false;
    }
    if (!err) {
      array.push(comment);
      localStorage.setItem("comment",JSON.stringify(array));
      this.$router.navigate(['CommentList']);
    } else {
      alert("You can only add one comment per user!");
    }
  }

  this.goTo = function(page) {
    if (page == 'view') this.$router.navigate(['CommentList']);
  }
}
})(window.angular);
